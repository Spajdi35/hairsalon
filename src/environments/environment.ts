// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB6THDhxK3oWVMTH7jgBhQxttYEeX64-3c",
    authDomain: "frizerski-salon-mari.firebaseapp.com",
    databaseURL: "https://frizerski-salon-mari.firebaseio.com",
    projectId: "frizerski-salon-mari",
    storageBucket: "",
    messagingSenderId: "439144360411",
    appId: "1:439144360411:web:8c4f00385bb861f2d29ffe",
    measurementId: "G-J8XCDBPJSL"
  }
};

// const firebaseConfig = {
//   apiKey: "AIzaSyB6THDhxK3oWVMTH7jgBhQxttYEeX64-3c",
//   authDomain: "frizerski-salon-mari.firebaseapp.com",
//   databaseURL: "https://frizerski-salon-mari.firebaseio.com",
//   projectId: "frizerski-salon-mari",
//   storageBucket: "",
//   messagingSenderId: "439144360411",
//   appId: "1:439144360411:web:8c4f00385bb861f2d29ffe",
//   measurementId: "G-J8XCDBPJSL"
// };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
