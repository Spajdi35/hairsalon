import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { InformationService } from 'src/app/information.service';
import { WorkingDay } from 'src/app/working-day/working-day.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-working-hours',
  templateUrl: './working-hours.component.html',
  styleUrls: ['./working-hours.component.css']
})
export class WorkingHoursComponent implements OnInit {
  workingDays: any;

  constructor(private infoService: InformationService) { }

  ngOnInit() {
    this.infoService.getWorkingDays().snapshotChanges().pipe(
      map(changes => 
        changes.map(c => 
          ({ key: c.payload.key, ...c.payload.val()})
        )
      )
    ).subscribe(workingDays => {
      this.workingDays = workingDays;
    });
  }

  onSaveTapped(workingDay: WorkingDay, startTimeInput: HTMLInputElement, endTimeInput: HTMLInputElement) {
    console.log(workingDay.name)
    this.infoService.updateUsluga(workingDay, startTimeInput.value, endTimeInput.value);
  }

}
