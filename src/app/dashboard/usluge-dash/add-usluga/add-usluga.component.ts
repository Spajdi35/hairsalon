import { Component, OnInit } from '@angular/core';
import { UslugaService } from 'src/app/usluge/usluga.service';
import { Usluga } from 'src/app/usluge/usluga.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-usluga',
  templateUrl: './add-usluga.component.html',
  styleUrls: ['./add-usluga.component.css']
})
export class AddUslugaComponent implements OnInit {

  constructor(private uslugaService: UslugaService, private location: Location) { }

  ngOnInit() {
  }

  onAddUsluga(nameInput: HTMLInputElement, lowerPrice: HTMLInputElement, upperPrice: HTMLInputElement) {
    let name = nameInput.value;
    let lower = +lowerPrice.value;
    let upper = +upperPrice.value;

    this.uslugaService.addUsluga(name, lower, upper).then(ref => {
      console.log(ref.key);
    })
  }

  goBack() {
    this.location.back();
  }

}
