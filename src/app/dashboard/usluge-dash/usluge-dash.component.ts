import { Component, OnInit } from '@angular/core';
import { Usluga } from 'src/app/usluge/usluga.model';
import { UslugaService } from 'src/app/usluge/usluga.service';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-usluge-dash',
  templateUrl: './usluge-dash.component.html',
  styleUrls: ['./usluge-dash.component.css']
})
export class UslugeDashComponent implements OnInit {
  usluge: any;

  constructor(private uslugaService: UslugaService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.uslugaService.getUsluge().snapshotChanges().pipe(
      map(changes => 
        changes.map(c => 
          ({ key: c.payload.key, ...c.payload.val()})
        )
      )
    ).subscribe(usluge => {
      this.usluge = usluge;
    });
  }

  onDelete(usluga: Usluga) {
    console.log(usluga.name);
    this.uslugaService.deleteUsluga(usluga);
  }

  onAdd() {
    this.router.navigate(['dodaj'], { relativeTo: this.route });
  }

  onChangeUsluga(usluga: Usluga) {
    this.uslugaService.uslugaToBeChanged = usluga;
    this.router.navigate(['promjeni', usluga.name], { relativeTo: this.route });
  }

}
