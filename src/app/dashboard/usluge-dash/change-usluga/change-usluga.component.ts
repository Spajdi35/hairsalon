import { Component, OnInit, Input } from '@angular/core';
import { Usluga } from 'src/app/usluge/usluga.model';
import { ActivatedRoute, Params } from '@angular/router';
import { UslugaService } from 'src/app/usluge/usluga.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-change-usluga',
  templateUrl: './change-usluga.component.html',
  styleUrls: ['./change-usluga.component.css']
})
export class ChangeUslugaComponent implements OnInit {
  usluga: Usluga;

  constructor(private route: ActivatedRoute, private uslugeService: UslugaService, private location: Location) { }

  ngOnInit() {
    this.usluga = this.uslugeService.uslugaToBeChanged;
  }

  onSaveUsluga(usluga: Usluga, lowerPriceInput: HTMLInputElement, upperPriceInput: HTMLInputElement) {
    this.uslugeService.updateUsluga(usluga, +lowerPriceInput.value, +upperPriceInput.value);
  }

  goBack() {
    this.location.back();
  }

}
