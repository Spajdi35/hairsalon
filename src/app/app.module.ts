import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Router, Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { InformationService } from './information.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WorkingHoursComponent } from './dashboard/working-hours/working-hours.component';
import { UslugeComponent } from './usluge/usluge.component';
import { UslugeDashComponent } from './dashboard/usluge-dash/usluge-dash.component';
import { UslugaService } from './usluge/usluga.service';
import { AddUslugaComponent } from './dashboard/usluge-dash/add-usluga/add-usluga.component';
import { ChangeUslugaComponent } from './dashboard/usluge-dash/change-usluga/change-usluga.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginService } from './login/login.service';
import { AuthGuard } from './auth-guard.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    DashboardComponent,
    WorkingHoursComponent,
    UslugeComponent,
    UslugeDashComponent,
    AddUslugaComponent,
    ChangeUslugaComponent,
    ContactComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule, 
    FormsModule,
    NgbModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  providers: [InformationService, UslugaService, LoginService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
