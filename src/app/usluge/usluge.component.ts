import { Component, OnInit } from '@angular/core';
import { UslugaService } from './usluga.service';
import { Usluga } from './usluga.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-usluge',
  templateUrl: './usluge.component.html',
  styleUrls: ['./usluge.component.css']
})
export class UslugeComponent implements OnInit {
  usluge: any;

  constructor(private uslugaService: UslugaService) { }

  ngOnInit() {
    this.uslugaService.getUsluge().snapshotChanges().pipe(
      map(changes => 
        changes.map(c => 
          ({ key: c.payload.key, ...c.payload.val()})
        )
      )
    ).subscribe(usluge => {
      this.usluge = usluge;
    });
  }

}
