import { Usluga } from './usluga.model';
import { EventEmitter, Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FirebaseListObservable } from '@angular/fire/database-deprecated';
import { stringify } from '@angular/compiler/src/util';

@Injectable({
    providedIn: 'root'
})
export class UslugaService {
    
    private dbPath = '/usluge';

    private uslugeRef: AngularFireList<Usluga> = null;

    uslugaToBeChanged: any;

    constructor(private db: AngularFireDatabase) {
        this.uslugeRef = db.list(this.dbPath);
    }

    getUsluge() {
        return this.uslugeRef;
    }

    addUsluga(name: string, lowerPrice: number, upperPrice: number) {
        let usluga = new Usluga(name, lowerPrice, upperPrice);
        return this.uslugeRef.push(usluga);
    }

    updateUsluga(usluga: Usluga, lowerPrice: number, upperPrice: number) {
        usluga.lowerPrice = lowerPrice;
        usluga.upperPrice = upperPrice;

        return this.uslugeRef.update(usluga.key, usluga);
    }

    deleteUsluga(usluga: Usluga) {
        console.log(usluga.key);
        return this.uslugeRef.remove(usluga.key);
    }

}