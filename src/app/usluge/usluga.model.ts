export class Usluga {
    public key?: string;

    constructor(public name: string, public lowerPrice: number, public upperPrice: number) {}

}