export class WorkingDay {
    public key?: string; 

    constructor(public name: string, public startTime: string, public endTime: string) {}

}