import { Injectable } from '@angular/core';
import { resolve } from 'url';
import { reject } from 'q';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { LoginComponent } from './login.component';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    userData: Observable<firebase.User>;

    isLoggedIn = false;

    constructor(private angularFireAuth: AngularFireAuth, private router: Router) {
        this.userData = angularFireAuth.authState;
    }

    logIn(email: string, password: string, loginComponent: LoginComponent) {
        return this.angularFireAuth
        .auth
        .signInWithEmailAndPassword(email, password)
        .then(res => {
            console.log('Successfully signed in!');
            this.isLoggedIn = true;
            this.router.navigate(['/dashboard']);
        })
        .catch(err => {
            console.log('Something is wrong:', err.message);
            this.isLoggedIn = false;
            this.router.navigate(['/contact']);
        });
    }

    logOut() {
        this.isLoggedIn = false;
        this.angularFireAuth
        .auth
        .signOut();
    }



}