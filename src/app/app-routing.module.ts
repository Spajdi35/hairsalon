import { Routes, RouterModule, Router } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { UslugeComponent } from "./usluge/usluge.component";
import { ContactComponent } from "./contact/contact.component";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { WorkingHoursComponent } from "./dashboard/working-hours/working-hours.component";
import { UslugeDashComponent } from "./dashboard/usluge-dash/usluge-dash.component";
import { AddUslugaComponent } from "./dashboard/usluge-dash/add-usluga/add-usluga.component";
import { ChangeUslugaComponent } from "./dashboard/usluge-dash/change-usluga/change-usluga.component";
import { NgModule } from '@angular/core';
import { AuthGuard } from './auth-guard.service';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'usluge', component: UslugeComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'admin', component: LoginComponent },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/workingHours', component: WorkingHoursComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/usluge', component: UslugeDashComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/usluge/dodaj', component: AddUslugaComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/usluge/promjeni/:id', component: ChangeUslugaComponent, canActivate: [AuthGuard] }
]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {

}