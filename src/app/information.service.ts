import { EventEmitter, Injectable } from '@angular/core';
import { WorkingDay } from './working-day/working-day.model';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';

@Injectable({
    providedIn: 'root'
})
export class InformationService {
    private dbPath = '/radniSati';

    private workingDaysRef: AngularFireList<WorkingDay> = null;

    constructor(private db: AngularFireDatabase) {
        this.workingDaysRef = db.list(this.dbPath)
    }
    
    getWorkingDays() {
        return this.workingDaysRef;
    }

    updateUsluga(workingDay: WorkingDay, opening: string, closing: string) {
        workingDay.startTime = opening;
        workingDay.endTime = closing;

        return this.workingDaysRef.update(workingDay.key, workingDay);
    }

}