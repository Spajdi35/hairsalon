import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import 'bootstrap';
import { InformationService } from '../information.service';
import { WorkingDay } from '../working-day/working-day.model';
import { UslugaService } from '../usluge/usluga.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  workingDays: any;

  constructor(private infoService: InformationService, private uslugeService: UslugaService) {}

  ngOnInit() {
    this.infoService.getWorkingDays().snapshotChanges().pipe(
      map(changes => 
        changes.map(c => 
          ({ key: c.payload.key, ...c.payload.val()})
        )
      )
    ).subscribe(workingDays => {
      this.workingDays = workingDays;
    });
  }

}
